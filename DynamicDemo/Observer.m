//
//  Observer.m
//  DynamicDemo
//
//  Created by James Cash on 11-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "Observer.h"

@implementation Observer

- (void)handleClickHappening
{
    NSLog(@"In observer, was notified of click");
}

- (void)secretMethod
{
    NSLog(@"Ssh, I'm secret");
}

@end
