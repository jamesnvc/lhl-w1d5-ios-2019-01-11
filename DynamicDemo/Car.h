//
//  Car.h
//  DynamicDemo
//
//  Created by James Cash on 11-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class Car;

@protocol CarDriver <NSObject>

- (void)handleObstacle:(Car*)car roadConditions:(NSString*)string;

@optional

- (void)driveVeryVeryFast:(Car*)car speedLimit:(NSInteger)limit;

@end

@interface Car : NSObject

@property (nonatomic,weak) id<CarDriver> delegate;

- (void)drive;

@end


NS_ASSUME_NONNULL_END
