//
//  NormalPerson.h
//  DynamicDemo
//
//  Created by James Cash on 11-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"

NS_ASSUME_NONNULL_BEGIN

@interface NormalPerson : NSObject <CarDriver>

@end

NS_ASSUME_NONNULL_END
