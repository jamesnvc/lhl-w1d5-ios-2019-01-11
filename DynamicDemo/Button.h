//
//  Button.h
//  DynamicDemo
//
//  Created by James Cash on 11-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Button : NSObject


- (void)setTarget:(id)target forAction:(SEL)action;

- (void)click;

@end

NS_ASSUME_NONNULL_END
