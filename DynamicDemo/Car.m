//
//  Car.m
//  DynamicDemo
//
//  Created by James Cash on 11-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "Car.h"

@implementation Car

- (void)drive
{
    NSLog(@"car: starting driving %@", self);
    NSLog(@"car: Oh no, something's on the road");
    [self.delegate handleObstacle:self roadConditions:@"snowy"];

    NSLog(@"car: Road is clear, if you can drive very quickly");
    if ([self.delegate respondsToSelector:@selector(driveVeryVeryFast:speedLimit:)]) {
        [self.delegate driveVeryVeryFast:self speedLimit:100];
        NSLog(@"car:Zoom!");
    } else {
        NSLog(@"car: No, just drive carefully");
    }
    NSLog(@"done");
}

@end
