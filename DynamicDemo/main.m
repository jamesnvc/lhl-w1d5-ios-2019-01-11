//
//  main.m
//  DynamicDemo
//
//  Created by James Cash on 11-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Observer.h"
#import "Button.h"
#import "NSString+Emoji.h"
#import "Car.h"
#import "NormalPerson.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
 
//        [@"some string" foobar];

//        SEL fooSel = @selector(foobar);
//        [@"some string" performSelector:fooSel];

        /*
        if (YES) {
            [someObject method1];
        } else {
            [someObject method2];
        }

        SEL someSel;
        if (YES) { someSel = @selector(method1); }
        else { someSel = @selector(method2); }
        [someObject performSelector:someSel];
        */

        Observer* obs = [[Observer alloc] init];

        Button *btn = [[Button alloc] init];
        [btn setTarget:obs forAction:@selector(handleClickHappening)];
        NSLog(@"Button set up");
        // pretend this is actually the user clicking something
        [btn click];


        // dynamic selectors

        [obs performSelector:@selector(secretMethod)];

        NSArray *array1;
        if (arc4random_uniform(10) < 5) {
            array1 = [NSMutableArray arrayWithArray:@[@"a", @"b", @"c"]];
        } else {
            array1 = @[@"a", @"b", @"c"];
        }
        // [array1 addObject:@"d"]
        if ([array1 respondsToSelector:@selector(addObject:)]) {
            [array1 performSelector:@selector(addObject:) withObject:@"d"];
        } else {
            NSLog(@"immutable array, not adding");
        }
        NSLog(@"array = %@", array1);


        // categories

        // want:
        //[@"some string" emojify]; // => @"some string 💩"

        // could subclass NSString to make MyString and put that method there...
        // but then I would to do [[MyString alloc] initWithString:@" ...."]
        // because a literal like @"some string" is always an NSString

        NSLog(@"my string: %@", [@"some string" emojify]);

        // delegation
        NormalPerson *person = [[NormalPerson alloc] init];
        Car* car = [[Car alloc] init];
        NSLog(@"created car: %@", car);
        car.delegate = person;
        [car drive];


        // nsnumber & nsvalue
        NSNumber *n = @(10 + 5), *f = @(15.123);
        double a = f.doubleValue + n.integerValue;
        NSLog(@"n + f = %f", [f doubleValue] + [n floatValue]);

        NSArray *numbers = @[@(15), @(19.123), @(7), @(a)];
        NSLog(@"Numbers %@", numbers);

        NSPoint myPoint;
        myPoint.x = 15.15;
        myPoint.y = 110.0;

        NSValue *pointValue = [NSValue valueWithPoint:myPoint];
        [pointValue pointValue].x;

    }
    return 0;
}
