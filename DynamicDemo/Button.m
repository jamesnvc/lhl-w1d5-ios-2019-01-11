//
//  Button.m
//  DynamicDemo
//
//  Created by James Cash on 11-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "Button.h"

@interface Button ()
@property (nonatomic,weak) id target;
@property (nonatomic,assign) SEL action;
@end

@implementation Button

- (void)setTarget:(id)target forAction:(SEL)action {
    self.target = target;
    self.action = action;
}

// pretend there's a bunch of logic here to handle input & determine when it was clicked
- (void)click {
    [self.target performSelector:self.action];
}

@end
