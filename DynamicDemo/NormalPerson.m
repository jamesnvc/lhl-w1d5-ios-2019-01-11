//
//  NormalPerson.m
//  DynamicDemo
//
//  Created by James Cash on 11-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "NormalPerson.h"

@implementation NormalPerson

- (void)handleObstacle:(Car *)car
        roadConditions:(NSString *)conditions {
    NSLog(@"driver: Carefully swerve around the obstacle in %@ on %@ roads", car, conditions);
}

@end
